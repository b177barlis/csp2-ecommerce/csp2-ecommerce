const mongoose = require("mongoose");
const productSchema = new mongoose.Schema({
	name : {
		type : String,
		required : false
	},
	author : {
		type : String,
		required : false
	},
	description : {
		type : String,
		required : false
	},
	price : {
		type : Number,
		required : false
	},
	category : {
		type : String,
		required : false
	},
	isActive : {
		type : Boolean,
		default : true
	},
	createdOn : {
		type : Date,
		default : new Date()
	},
	buyers : [
		{
			userId : {
				type : String,
				default : true
		},
			purchasedOn : {
				type : Date,
				default : new Date()
		}
	}]

})

module.exports = mongoose.model('products', productSchema);