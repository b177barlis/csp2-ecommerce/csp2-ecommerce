const mongoose = require("mongoose");
const userSchema = new mongoose.Schema({
	fullName : {
		type : String,
		required : [true, "Full Name is required"]
	},
	email : {
		type : String,
		required : [true, "Email is required"]
	},
	password : {
		type : String,
		required : [true, "Password is required"]
	},
	isAdmin : {
		type : Boolean,
		default : false
	},
	orders : [
			{
			purchasedOn : {
			type : Date,
			default : new Date()
			},
			productId : {
			type : String,
			required : [true, "productId is required"]
			}
		}]
})

module.exports = mongoose.model("users", userSchema);