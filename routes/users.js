const express = require("express");
const router = express.Router();
const userController = require("../controllers/users");
const auth = require("../auth");


router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
})

router.get("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})

router.put("/:userId/setAsAdmin", auth.verify, (req, res) => {

    const data = {
        userId : req.params.userId,
        isAdmin : auth.decode(req.headers.authorization).isAdmin
    }

	userController.settingAdmin(data).then(resultFromController => res.send(resultFromController));
})


router.post("/checkout", auth.verify, (req, res) => {
    
    const data = {
        userId : req.body.userId,
        productId : req.body.productId,
        isAdmin : auth.decode(req.headers.authorization).isAdmin
    }
    userController.creatingOrder(data).then(resultFromController => res.send(resultFromController));
})


router.get("/orders", auth.verify, (req, res) => {

    const data = {
        userId : req.body.userId,
        productId : req.body.productId,
        isAdmin : auth.decode(req.headers.authorization).isAdmin
    }

    userController.getOrders(data).then(resultFromController => res.send(resultFromController));
});

router.get("/myOrders/:userId", (req, res) => {
    userController.getMyOrders(req.params).then(resultFromController => res.send(resultFromController));
})





module.exports = router;
