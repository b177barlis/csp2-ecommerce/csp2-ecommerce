const express = require("express");
const router = express.Router();
const productsController = require("../controllers/products");
const auth = require("../auth");


// Posting new products

router.post("/newproduct", auth.verify, (req, res) => {
	productsController.addProducts(req.body).then(resultFromController => res.send(resultFromController));
})

// Getting all products
router.get("/all", (req, res) => {
	productsController.getAllProducts().then(resultFromController => res.send(resultFromController));
})

// Getting all active products
router.get("/", (req, res) => {
    productsController.getProductsActive().then(resultFromController => res.send(resultFromController))
})



// Getting specific product
router.get("/:productId", (req, res) => {
	productsController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
})


// Archiving product
router.put("/:productId/archive", auth.verify, (req, res) => {

    const productArchive = {
        productId : req.params.productId,
        isAdmin : auth.decode(req.headers.authorization).isAdmin
    }

	productsController.productArchive(productArchive).then(resultFromController => res.send(resultFromController));
})

module.exports = router;
