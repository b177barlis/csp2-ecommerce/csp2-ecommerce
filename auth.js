const jwt = require('jsonwebtoken');


// User defined string data that will be used to create JSON web tokens
const secret = "eCommerceAPI";


// Token creation
module.exports.createAccessToken = (user) => {
	const data = {
		id : user._id,
		email : user.email,
		isAdmin : user.isAdmin
	};

	return jwt.sign(data, secret, {});
}


// Token verification
module.exports.verify = (req, res, next) => {
	// The token is retrieved from the request header
	let token = req.headers.authorization;

	// Token received and is not undefined
	if(typeof token !== "undefined"){
		console.log(token);

		token = token.slice(7, token.length);

		// Validate the token using "verify" method decrypting the token using the secret code
		return jwt.verify(token, secret, (err, data) => {
			// If JWT is not valid
			if(err){
				return res.send({auth : "failed"});
			}
			// If JWT is valid
			else{
				// This will allow the application to proceed with the next middleware function/callback function in the route
				next();
			}
		})
	}
	else{
		return res.send({auth: "failed"});
	}
}

// Token decryption
module.exports.decode = (token) => {
	// Token received in not undefined
	if(typeof token !== "undefined"){
		// Retrieves only the token and removes the "Bearer" prefix
		token = token.slice(7,token.length);

		return jwt.verify(token, secret, (err,data) => {
			if(err){
				return null;
			}
			else{
				// The "decode" method is used to obtain the information from JWT
				// The "{complete:true}" option allows us to return additional information from the JWT
				// The payload contains information provided in the "createAccessToken"
				return jwt.decode(token, {complete: true}).payload
			}
		})
	}
	else{
			return null;
	}
}