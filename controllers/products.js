const products = require("../models/products");
const orders = require("../models/orders");
const auth = require('../auth');

module.exports.addProducts = (reqBody) => {
	let newProducts = new products({
		name : reqBody.name,
		author : reqBody.author,
		description : reqBody.description,
		price : reqBody.price,
		category : reqBody.category,
		isActive : reqBody.isActive
	})


	return newProducts.save().then((products, error) => {
		if(error){
			return false;
		}
		else{
			return true;
		};
	})

}

module.exports.getAllProducts = () => {
	return products.find({}).then(result => {
		return result;
	})
} 


module.exports.getProduct = (reqParams) => {
	return products.findById(reqParams.productId).then(result => {
		return result;
	})
}

module.exports.getProductsActive = () => {
	return products.find({isActive : true}).then(result => {
        return result
    })
}


module.exports.productArchive = (productArchive) => {

    if (productArchive.isAdmin) {
        let updateActive = {
            isActive : false
        };

        return products.findByIdAndUpdate(productArchive.productId, updateActive).then((user, error) => {
            if (error) {
                return false;
            } 
            else {

                return true;

            }

        })
    } 
    else {
		return Promise.resolve(false);
	}

}