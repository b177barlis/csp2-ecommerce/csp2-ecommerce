const users = require("../models/users");
const products = require("../models/products");
const orders = require("../models/orders");
const bcrypt = require("bcrypt");
const auth = require("../auth");


module.exports.registerUser = (reqBody) => {
	let newUser = new users({
		fullName : reqBody.fullName,
		email : reqBody.email,
		password : bcrypt.hashSync(reqBody.password, 10),
		isAdmin : reqBody.isAdmin
	})

	// Saves the created object to our database
	return newUser.save().then((user, error) => {
		// User registration failed
		if(error){
			return false;
		}
		// User registration successful
		else{
			return true;
		}
	})
}


module.exports.loginUser = (reqBody) => {
	return users.findOne({email : reqBody.email}).then(result => {
		// User does not exist
		if(result == null){
			return false;
		}
		// User exists
		else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if(isPasswordCorrect){
				// Generate an access token
				return { access : auth.createAccessToken(result)}
			}
			// Passwords do not match
			else{
				return false;
			}
		}
	})
}


module.exports.settingAdmin = (data) => {

    if (data.isAdmin) {
        let updateAdmin = {
            isAdmin : true
        };

        return users.findByIdAndUpdate(data.userId, updateAdmin).then((user, error) => {
            if (error) {
                return false;
            } 
            else {

                return true;

            }

        })
    } 
    else {
		return Promise.resolve(false);
	}

}


// Creating order
module.exports.creatingOrder = async(data) => {
    if(data.isAdmin == false) {
		let isUserUpdated = await users.findById(data.userId).then(users => {
            users.orders.push({productId : data.productId});
    
            return users.save().then((user, error) => {
                if(error) {
                    return false;
                }
                else{
                    return true;
                }
            })
        })

		let isProductUpdated = await products.findById(data.productId).then(products => {
            products.orders.push({userId : data.userId});

            return products.save().then((product, error) => {
                if(error) {
                    return false;
                }
                else{
                    return true;
                }
            })
        })
		let isOrderUpdated = await products.findById(data.productId).then(products => {
        	
        	let newOrder = new orders({
                    userId: data.userId,
                    productId: data.productId,
                    purchasedOn: new Date()
                })
                return newOrder.save().then((products, error) => {
                    if(error){
                        return false
                    }
                    else {
                        return true
                    }
                })
        })
		if(isUserUpdated && isProductUpdated && isOrderUpdated) {
            return true;
        }
        else {
            return false;
        }
    }
    else {
        return Promise.resolve(false);
    }     
}


// Retrieve All Orders (Admin)
module.exports.getOrders = (data) => {
	if(data.isAdmin) {
		return orders.find({}).then(result => {
				return result;
		})
	}
	else {
		return false;
	}
};


// Retrieve My Orders (Non-Admin)
module.exports.getMyOrders = (reqParams) => {
	return users.findById(reqParams.userId).then(result => {
		return result;
	})
} 


